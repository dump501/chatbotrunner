import React, { useState } from "react";
import Chatbot from "react-chatbot-kit";


import './App.css';
import config from "./chatbot/Config";
import ActionProvider from "./chatbot/ActionProvider";
import MessageParser from "./chatbot/MessageParser";


function App() {
  return (
      <div className="App">
          <div style={{ maxWidth: "300px" }}>
              <Chatbot config={config} actionProvider={ActionProvider} messageParser={MessageParser} />
          </div>
      </div>
  );
}

export default App;
