import React from 'react'

//import './Options.css';

const Options = (props)=> {
    const options = [

        {
            text: "school general infos",
            handler: props.actionProvider.handleSchoolGeneralInfos,
            id: 1
        },
        {
            text: "login / register",
            handler: props.actionProvider.handleLogin,
            id: 2
        },
    ];
    const buttonsMarkup = options.map((option)=>(
        <button key={option.id} onClick={option.handler} className="option-button">
            {option.text}
        </button>
    ))
    return <div className="options-container">{buttonsMarkup}</div>;
}

export default Options
