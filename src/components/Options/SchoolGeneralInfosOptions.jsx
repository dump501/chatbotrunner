import React from 'react'

const SchoolGeneralInfosOptions = (props) => {

    const options = [

        {
            text: "open hours",
            handler: props.actionProvider.handleOpenHours,
            id: 1
        },
        {
            text: "school staff",
            handler: props.actionProvider.handleSchoolStaff,
            id: 2
        },
        {
            text: "school options",
            handler: props.actionProvider.handleSchoolOptions,
            id: 3
        },
    ];
    const buttonsMarkup = options.map((option)=>(
        <button key={option.id} onClick={option.handler} className="option-button">
            {option.text}
        </button>
    ))
    return (
        <div className="options-container">{buttonsMarkup}</div>
    )
}

export default SchoolGeneralInfosOptions
