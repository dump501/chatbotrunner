import {createChatBotMessage} from "react-chatbot-kit";

import Options from "../components/Options/Options";
import SchoolGeneralInfosOptions from "../components/Options/SchoolGeneralInfosOptions";
import Quiz from "../components/Quiz/Quiz";
import OpenHours from "../components/widgets/OpenHours/OpenHours";


const config = {
    botName: "SchoolBot",
    initialMessages: [createChatBotMessage('hi!, I\'m shoolBot what can I do for you? type help if you need help',{
        widget: "options",
    })],
    widgets: [
        {
            widgetName: "options",
            widgetFunc: (props) => <Options {...props} />,
        },
        {
            widgetName: "javascriptQuiz",
            widgetFunc: (props)=> <Quiz {...props} />,
            props: {
                questions: [
                    {
                        question: "what is closure ?",
                        answer: "Closure is a way for a function to retain access to it's enclosing function",
                        id: 1
                    },
                    {
                        question: "Explain prototypal inheritance",
                        answer: "protocol inhritance is a link between an object and an object store",
                        id: 2
                    },
                ]
            }
        },
        {
            widgetName: "OpenHourOptions",
            widgetFunc: (props) => <OpenHours {...props} />
        },
        {
            widgetName: "SchoolGeneralInfosOptions",
            widgetFunc: (props) => <SchoolGeneralInfosOptions {...props} />
        }
    ]
}

export default config;
