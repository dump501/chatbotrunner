class ActionProvider{
    constructor(createChatBotMessage, setStateFunc){
        this.createChatBotMessage = createChatBotMessage;
        this.setState = setStateFunc;
    }

    // handle chatbot responses fromm message parser
    /**
     * handle javascript quiz method
     */
    // handleJavascriptQuiz = () => {
    //     const message = this.createChatBotMessage("Fantastic! here is your quiz. Good Luck !",
    //     {
    //         widget: "javascriptQuiz",
    //     }
    //     );

    //     this.addChatbotToState(message);
    // }

    /**
     * handle open hours
     */
     handleOpenHours = () => {
        const message = this.createChatBotMessage("Our school open its door from monday to friday at 7 h 30 am and close it at 15 h pm", {
            widget: "SchoolGeneralInfosOptions",
        });
        this.addChatbotToState(message);
    }

    /**
     * schoool general infos
     */
    handleSchoolGeneralInfos = () => {
        const message = this.createChatBotMessage("Here are the genral school information :)", {
            widget: "SchoolGeneralInfosOptions",
        });
        this.addChatbotToState(message);
    }

    /**
     * handle school staff
     */
    handleSchoolStaff = () =>{
        const message = this.createChatBotMessage(
            "Our school have the best staff that ever exist in cameroon. It is made up of a principal whose name is Mr Kamdem Fontcha Cesar," +
            "We have 5 vice principl, 8 displine head master, 49 teachers",
            {
                widget: "SchoolGeneralInfosOptions"
            });
        this.addChatbotToState(message);
    }

    /**
     * handle login
     */
    handleLogin = () => {
        const message = this.createChatBotMessage("Enter your email");
        this.addChatbotToState(message);
    }

    /**
     * handle school options
     */
    handleSchoolOptions = () => {
        const message = this.createChatBotMessage("Here are the school options :)", {
            widget: "options",
        });
        this.addChatbotToState(message);
    }

    /**
     * handle help
     */
    handleHelp = () => {
        const message = this.createChatBotMessage("To converse with me you should respect some principle" +
        "you have to click on the button below the message I sended. Also, you can use the text input to provide information if asked",
        {
            widget:"options",
        });
        this.addChatbotToState(message);
    }

    /**
     * send a greet message
     */
    greet = ()=>{
        const message = this.createChatBotMessage("hello friend.");
        this.addChatbotToState(message);
    }

    /**
     * add message to chatbot
     *
     * @param {*} message
     */
    addChatbotToState = (message)=>{
        this.setState((prevState) => ({
            ...prevState,
            messages: [...prevState.messages, message],
        }));
    }
}

export default ActionProvider;
