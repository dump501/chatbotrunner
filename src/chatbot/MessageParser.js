class MessageParser {
    constructor(actionProvider) {
      this.actionProvider = actionProvider;
      // State represents the chatbot state and is passed
      // in at initalization. You can use it to read chatbot state
      // inside the messageParser
    }

    parse = (message) => {
        console.log('message');
        const lowercase = message.toLowerCase();

        if(lowercase.includes("hello"))
        {
            this.actionProvider.greet();
        }

        if(lowercase.includes("javascript") || lowercase.includes("js")){
            this.actionProvider.handleJavascriptQuiz();
        }

        if(lowercase.includes("help")){
            this.actionProvider.handleHelp();
        }
    };
  }

  export default MessageParser;
